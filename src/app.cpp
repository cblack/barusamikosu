#include <QDir>
#include <QStandardPaths>
#include <QTimer>
#include <QRandomGenerator>

#include <memory>
#include <unistd.h>

#include "app.h"
#include "bdatabase.h"
#include "wrappers.h"

BApp::BApp()
{
    instance = this;

    // timer.reset(new QTimer);
    // QObject::connect(timer.get(), &QTimer::timeout, timer.get(), [=]() {
    //     documents[0]->document->setProperty(1, "x", QRandomGenerator::global()->bounded(50, 100));
    // });
    // timer->setInterval(500);
    // timer->start();
    engine.reset(new QQmlEngine);
    windowComponent.reset(new QQmlComponent(engine.get()));

    QUrl windowQml("qrc:/Window.qml");
    QFile it(":/Window.qml");
    Q_ASSERT(it.open(QFile::ReadOnly));
    const auto data = it.readAll();

    windowComponent->setData(data, windowQml);
}

BApp::~BApp()
{

}

BApp* BApp::instance;

void
BApp::start()
{
    documents.push_back(std::make_unique<BOpenDocument>(std::make_unique<BDatabase>("design.sqlite")));
    documents[0]->document->database->open().waitForFinished();

    auto& document = documents[0]->document;
    auto& database = document->database;

    WBounding rect1(document, 1);
    WColor color1(document, 1);
    WBounding rect2(document, 2);
    WColor color2(document, 2);

    database->createObject(1, 0, "0", "rectangle").waitForFinished();
    rect1.setRect({50, 50, 50, 50});
    color1.setColor(Qt::red);

    database->createObject(2, 1, "z", "rectangle").waitForFinished();
    rect2.setRect({25, 25, 50, 50});
    color2.setColor(Qt::green);

    auto win = qobject_cast<QQuickWindow*>(windowComponent->create());
    qWarning().noquote() << windowComponent->errorString();
    documents[0]->window = win;
}
