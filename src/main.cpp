#include <QApplication>
#include <QQmlEngine>

#include "app.h"

int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

	QApplication app(argc, argv);
	BApp bApp;

	bApp.start();

	return QCoreApplication::exec();
}
