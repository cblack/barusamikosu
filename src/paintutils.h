#pragma once

#include <QPainter>

template<typename Rect>
inline void drawRect(QPainter* painter, const Rect& rect)
{
    const qreal adjustment = 0.5 * painter->pen().widthF();
    painter->drawRect(rect.adjusted(adjustment, adjustment, -adjustment, -adjustment));
}

template<typename Rect>
inline void drawRoundedRect(QPainter* painter, const Rect& rect, qreal radiusX, qreal radiusY)
{
    const qreal adjustment = 0.5 * painter->pen().widthF();
    painter->drawRoundedRect(rect.adjusted(adjustment, adjustment, -adjustment, -adjustment), radiusX, radiusY);
}
