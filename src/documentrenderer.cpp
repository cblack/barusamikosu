#include "documentrenderer.h"
#include "objectrenderer.h"
#include "app.h"

struct DocumentRenderer::Private
{
    int idx;
    QMap<quint32, ObjectRenderer*> objects;
};

DocumentRenderer::DocumentRenderer(QQuickItem* parent) : QQuickItem(parent), d(new Private)
{
}

DocumentRenderer::~DocumentRenderer()
{
}

void
DocumentRenderer::createForParent(quint32 parent, ObjectRenderer* itemParent)
{
    for (const auto& row : BApp::instance->documents[d->idx]->document->getChildrenOf(parent))
    {
        d->objects[row.id] = new ObjectRenderer(itemParent == nullptr ? (QQuickItem*)this : (QQuickItem*)itemParent);
        d->objects[row.id]->setForObject(row.id);
        d->objects[row.id]->setForDocument(d->idx);
        d->objects[row.id]->componentComplete();
        createForParent(row.id, d->objects[row.id]);
    }
}

void
DocumentRenderer::updateFromDocument()
{
    createForParent(0);
}

int
DocumentRenderer::forDocument() const
{
    return d->idx;
}

void
DocumentRenderer::setForDocument(int id)
{
    if (d->idx == id)
        return;

    d->idx = id;
    Q_EMIT forDocumentChanged();
}

void
DocumentRenderer::componentComplete()
{
    QQuickItem::componentComplete();
    updateFromDocument();
}
