#pragma once

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>
#include <QFuture>
#include <QThreadPool>

using quint32 = quint32;
using QString = QString;
using QVariant = QVariant;

struct BObjectRow
{
    quint32 id;
    quint32 parent;
    QString ord;
    QString kind;
};

struct QStringRow
{
    quint32 id;
    QString property;
    QVariant value;
};

inline QDebug& operator<<(QDebug& debug, const QStringRow &row)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << row.id << " " << row.property << " " << row.value;
    return debug;
}

inline QDebug& operator<<(QDebug& debug, const BObjectRow &row)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << row.id << " " << row.parent << " " << row.ord << " " << row.kind;
    return debug;
}

struct BMigration
{
    QString name;
    QString upQuery;
};

class BDatabase : public QObject
{

    Q_OBJECT

    QSqlDatabase database;
    QString databaseName;
    QList<BMigration> migrations;
    QThreadPool threadPool;
    QFuture<void> setupMigrations();
    QFuture<void> applyMigrations();

public:
    BDatabase(const QString& databaseName, QObject* parent = nullptr);
    ~BDatabase();

    QFuture<void> open();

    QFuture<void> createObject(quint32 id, quint32 parent, const QString& ord, const QString& kind);
    QFuture<QList<BObjectRow>> objectsOfParent(quint32 parent);

    QFuture<void> setPropertyOfObject(quint32 object, const QString& prop, const QVariant& value);
    QFuture<QVariant> propertyOfObject(quint32 object, const QString& prop);

    Q_SIGNAL void objectPropertyChanged(quint32 object, QString prop, QVariant value);
};
