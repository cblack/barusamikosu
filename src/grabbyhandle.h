#pragma once

#include <QQuickPaintedItem>

class GrabbyHandle : public QQuickPaintedItem
{

    Q_OBJECT

    QPoint previousPos;

public:
    explicit GrabbyHandle(QQuickItem* parent = nullptr);
    ~GrabbyHandle();

    void paint(QPainter* painter) override;

    void mousePressEvent(QMouseEvent* evt) override;
    void mouseMoveEvent(QMouseEvent* evt) override;
    void mouseReleaseEvent(QMouseEvent* evt) override;

    Q_SIGNAL void moved(const QVector2D& delta);

};
