#include <QColor>
#include "bdocument.h"

QList<BObjectRow> BDocument::getChildrenOf(quint32 id)
{
    return database->objectsOfParent(id);
}

QVariant BDocument::getProperty(quint32 id, const QString& prop)
{
    return database->propertyOfObject(id, prop);
}

BDocument::BDocument(std::unique_ptr<BDatabase> db) : database(std::move(db))
{

}

void
BDocument::setProperty(quint32 id, const QString& name, const QVariant& value)
{
    return database->setPropertyOfObject(id, name, value).waitForFinished();
}
