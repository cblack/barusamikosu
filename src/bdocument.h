#pragma once

#include "bdatabase.h"

class BDocument
{

public:
    std::unique_ptr<BDatabase> database;

    BObjectRow findObject(quint32 id)
    {
        Q_UNUSED(id)

        Q_UNIMPLEMENTED();

        return {};
    }

    QList<BObjectRow> getChildrenOf(quint32 id);
    QVariant getProperty(quint32 id, const QString& name);
    void setProperty(quint32 id, const QString& name, const QVariant& value);

    BDocument(std::unique_ptr<BDatabase> database);
};
