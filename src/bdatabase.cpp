#include <QDataStream>
#include <QtConcurrent>

#include "bdatabase.h"

const QString DRIVER = QLatin1String("QSQLITE");

BDatabase::BDatabase(const QString& databaseName, QObject* parent) : QObject(parent), database(), databaseName(databaseName), threadPool()
{
    threadPool.setMaxThreadCount(1);
    threadPool.setExpiryTimeout(-1);

    migrations << BMigration{"base-objects", R"SQL(
        CREATE TABLE Objects (
            id INTEGER PRIMARY KEY NOT NULL,
            parent INTEGER,
            ord TEXT NOT NULL,
            kind TEXT NOT NULL,
            FOREIGN KEY (parent) REFERENCES Objects(id)
        );
    )SQL"};
    migrations << BMigration{"properties", R"SQL(
        CREATE TABLE Properties (
            forObject INTEGER NOT NULL,
            property TEXT NOT NULL,
            pValue BLOB NOT NULL,
            FOREIGN KEY (forObject) REFERENCES Objects(id),
            UNIQUE(forObject, property)
        );
    )SQL"};
}

BDatabase::~BDatabase()
{

}

QFuture<void> BDatabase::open()
{
    return QtConcurrent::run(&threadPool, [=]() {
        Q_ASSERT(QSqlDatabase::isDriverAvailable(DRIVER));
        QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);
        db.setDatabaseName(databaseName);
        if (!db.open()) {
            qCritical() << db.lastError() << "while opening database at" << "design.sqlite";
        }
        database = db;

        setupMigrations().waitForFinished();
        applyMigrations().waitForFinished();
    });
}

QFuture<void> BDatabase::setupMigrations()
{
    return QtConcurrent::run(&threadPool, [=]() {
        if (database.tables().contains("_migrations")) {
            return;
        }

        const auto statement = QLatin1String(R"RJIENRLWEY(
            CREATE TABLE IF NOT EXISTS _migrations (
                name TEXT PRIMARY KEY NOT NULL
            )
        )RJIENRLWEY");

        auto query = QSqlQuery(database);
        if (!query.prepare(statement)) {
            qCritical() << query.lastError() << "while preparing migrations table creation";
        }
        if (!query.exec()) {
            qCritical() << query.lastError() << "while setting up migrations table";
        }
    });
}

QFuture<void> BDatabase::applyMigrations()
{
    return QtConcurrent::run(&threadPool, [this]() {
        const auto existsStatement = QLatin1String(R"SQL(
            SELECT EXISTS(SELECT 1 FROM _migrations WHERE name = :name);
        )SQL");
        const auto insertStatement = QLatin1String(R"SQL(
            INSERT INTO _migrations ( name ) VALUES ( :name );
        )SQL");

        for (const auto& migration : migrations)
        {
            QSqlQuery existsQuery(database);
            if (!existsQuery.prepare(existsStatement)) {
                qCritical() << existsQuery.lastError() << "preparing checks condition" << migration.name << "was previously applied";
            }
            existsQuery.bindValue(":name", migration.name);
            if (!existsQuery.exec()) {
                qCritical() << existsQuery.lastError() << "while checking if migration" << migration.name << "was previously applied";
            }
            if (!existsQuery.next()) {
                qCritical() << existsQuery.lastError() << "while getting result for checking if" << migration.name << "was previously applied";
            }
            if (existsQuery.value(0).toBool()) {
                continue;
            }

            QSqlQuery migrationQuery(database);
            if (!migrationQuery.prepare(migration.upQuery)) {
                qCritical() << migrationQuery.lastError() << "while preparing migration" << migration.name;
            }
            if (!migrationQuery.exec()) {
                qCritical() << migrationQuery.lastError() << "while trying to apply migration" << migration.name;
            }

            QSqlQuery doneQuery(database);
            if (!doneQuery.prepare(insertStatement)) {
                qCritical() << doneQuery.lastError() << "while preparing migration done marker for" << migration.name;
            }
            doneQuery.bindValue(":name", migration.name);
            if (!doneQuery.exec()) {
                qCritical() << doneQuery.lastError() << "while marking migration" << migration.name << "as done";
            }
        }
    });
}

QFuture<QList<BObjectRow>> BDatabase::objectsOfParent(quint32 parent)
{
    return QtConcurrent::run(&threadPool, [this, parent]() {
        QString statement;

        if (parent == 0) {
            statement = QLatin1String(R"SQL(
                SELECT * FROM Objects WHERE parent IS NULL;
            )SQL");
        } else {
            statement = QLatin1String(R"SQL(
                SELECT * FROM Objects WHERE parent = :parent;
            )SQL");
        }

        QSqlQuery query(database);
        if (!query.prepare(statement)) {
            qCritical() << query.lastError() << "while preparing fetching child objects from database";
            return QList<BObjectRow>();
        }

        if (parent != 0) {
            query.bindValue(":parent", parent);
        }

        if (!query.exec()) {
            qCritical() << query.lastError() << "while executing fetching child objects from database";
            return QList<BObjectRow>();
        }

        QList<BObjectRow> ret;
        ret.reserve(query.size());
        while (query.next()) {
            ret << BObjectRow{
                query.value(0).toUInt(),
                query.value(1).toUInt(),
                query.value(2).toString(),
                query.value(3).toString(),
            };
        }

        return ret;
    });
}

QFuture<QVariant> BDatabase::propertyOfObject(quint32 object, const QString& prop)
{
    return QtConcurrent::run(&threadPool, [this, object, prop = prop]() {
        const auto statement = QLatin1String(R"SQL(
            SELECT pValue FROM Properties WHERE forObject = :object AND property = :property;
        )SQL");

        QSqlQuery query(database);
        if (!query.prepare(statement)) {
            qCritical() << query.lastError() << "while preparing fetching property from database";
            return QVariant();
        }

        query.bindValue(":object", object);
        query.bindValue(":property", prop);

        if (!query.exec()) {
            qCritical() << query.lastError() << "while executing fetching property from database";
            return QVariant();
        }

        QVariant ret;
        if (!query.next()) {
            return QVariant();
        }

        const auto data = query.value(0).toByteArray();
        QDataStream stream(data);
        stream >> ret;

        return ret;
    });
}

QFuture<void> BDatabase::createObject(quint32 id, quint32 parent, const QString& ord, const QString& kind)
{
    return QtConcurrent::run(&threadPool, [this, id, parent, ord = ord, kind = kind]() {
        const auto statement = QLatin1String(R"SQL(
            INSERT INTO Objects (id, parent, ord, kind) VALUES (:id, :parent, :ord, :kind);
        )SQL");

        QSqlQuery query(database);
        if (!query.prepare(statement)) {
            qCritical() << query.lastError() << "while preparing create object";
            return;
        }

        query.bindValue(":id", id);
        if (parent == 0) {
            query.bindValue(":parent", QVariant::fromValue(nullptr));
        } else {
            query.bindValue(":parent", parent);
        }
        query.bindValue(":ord", ord);
        query.bindValue(":kind", kind);

        if (!query.exec()) {
            qCritical() << query.lastError() << "while executing create object";
        }
    });
}

QFuture<void> BDatabase::setPropertyOfObject(quint32 object, const QString& prop, const QVariant& value)
{
    return QtConcurrent::run(&threadPool, [this, object, prop = prop, value = value]() {
        const auto statement = QLatin1String(R"SQL(
            INSERT OR REPLACE INTO Properties (forObject, property, pValue) VALUES (:id, :prop, :value);
        )SQL");

        QSqlQuery query(database);
        if (!query.prepare(statement)) {
            qCritical() << query.lastError() << "while preparing set property of object";
            return;
        }

        QByteArray data;
        QDataStream strem(&data, QIODevice::WriteOnly);
        strem << value;

        query.bindValue(":id", object);
        query.bindValue(":prop", prop);
        query.bindValue(":value", data);

        if (!query.exec()) {
            qCritical() << query.lastError() << "while set property of object";
            return;
        }

        Q_EMIT objectPropertyChanged(object, prop, value);
    });
}
