#pragma once

#include <QQuickWindow>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQuickPaintedItem>

#include "bdatabase.h"
#include "bdocument.h"

struct BOpenDocument
{
    QQuickWindow* window;
    std::unique_ptr<BDocument> document;

    BOpenDocument(std::unique_ptr<BDatabase> database) : document(new BDocument(std::move(database)))
    {

    }
};

struct BApp
{
    static BApp* instance;

    std::vector<std::unique_ptr<BOpenDocument>> documents;
    QScopedPointer<QTimer> timer;
    QScopedPointer<QQmlEngine> engine;
    QScopedPointer<QQmlComponent> windowComponent;

    BApp();
    ~BApp();
    void start();
};
