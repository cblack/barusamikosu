#pragma once

#include <QQuickPaintedItem>
#include "app.h"

class ObjectRenderer;

class DocumentRenderer : public QQuickItem
{

    Q_OBJECT
    QML_ELEMENT

    struct Private;
    QScopedPointer<Private> d;

    void createForParent(quint32 parent, ObjectRenderer* itemParent = nullptr);
    void updateFromDocument();

public:
    explicit DocumentRenderer(QQuickItem* parent = nullptr);
    ~DocumentRenderer();

    Q_PROPERTY(int forDocument READ forDocument WRITE setForDocument NOTIFY forDocumentChanged REQUIRED)
    int forDocument() const;
    void setForDocument(int id);
    Q_SIGNAL void forDocumentChanged();

    void componentComplete() override;

};

