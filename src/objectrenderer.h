#pragma once

#include <QQuickPaintedItem>
#include "app.h"

class ObjectRenderer : public QQuickPaintedItem
{

    Q_OBJECT
    QML_ELEMENT

    struct Private;
    QScopedPointer<Private> d;

    void updateFromDocument();

public:
    explicit ObjectRenderer(QQuickItem* parent = nullptr);
    ~ObjectRenderer();

    Q_PROPERTY(int forDocument READ forDocument WRITE setForDocument NOTIFY forDocumentChanged REQUIRED)
    int forDocument() const;
    void setForDocument(int id);
    Q_SIGNAL void forDocumentChanged();

    Q_PROPERTY(quint32 forObject READ forObject WRITE setForObject NOTIFY forObjectChanged REQUIRED)
    quint32 forObject() const;
    void setForObject(quint32 id);
    Q_SIGNAL void forObjectChanged();

    void paint(QPainter* painter) override;
    void componentComplete() override;

};

