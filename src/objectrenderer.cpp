#include <QPainter>

#include "objectrenderer.h"
#include "grabbyhandle.h"
#include "paintutils.h"
#include "wrappers.h"

struct ObjectRenderer::Private
{
    quint32 id;
    int idx;
};

ObjectRenderer::ObjectRenderer(QQuickItem* parent) : QQuickPaintedItem(parent), d(new Private)
{
    auto handle = new GrabbyHandle(this);
    handle->setX(0 - handle->width()/2);
    handle->setY(0 - handle->height()/2);
    connect(handle, &GrabbyHandle::moved, this, [this](const QVector2D& delta) {
        WBounding bounding(BApp::instance->documents[d->idx]->document, d->id);

        bounding.setTopLeftX(bounding.topLeftX() + delta.x());
        bounding.setTopLeftY(bounding.topLeftY() + delta.y());
    });
}

ObjectRenderer::~ObjectRenderer()
{

}

void
ObjectRenderer::updateFromDocument()
{
    WBounding bounding(BApp::instance->documents[d->idx]->document, d->id);

    const auto rect = bounding.rect();

    setImplicitSize(rect.width(), rect.height());
    setPosition(rect.topLeft());
    update();
}


int
ObjectRenderer::forDocument() const
{
    return d->idx;
}

void
ObjectRenderer::setForDocument(int id)
{
    if (d->idx == id)
        return;

    d->idx = id;
    Q_EMIT forDocumentChanged();
}

quint32
ObjectRenderer::forObject() const
{
    return d->id;
}

void
ObjectRenderer::setForObject(quint32 id)
{
    if (d->id == id)
        return;

    d->id = id;
    Q_EMIT forObjectChanged();
}

void
ObjectRenderer::paint(QPainter* painter)
{
    WBounding bounding(BApp::instance->documents[d->idx]->document, d->id);
    WColor color(BApp::instance->documents[d->idx]->document, d->id);
    WRadius radius(BApp::instance->documents[d->idx]->document, d->id);

    painter->setPen(Qt::NoPen);
    painter->setBrush(color.color());
    drawRoundedRect(painter, bounding.zeroZeroRect(), radius.radius(), radius.radius());
}

void
ObjectRenderer::componentComplete()
{
    QQuickPaintedItem::componentComplete();
    updateFromDocument();

    auto doc = BApp::instance->documents[d->idx]->document->database.get();
    connect(doc, &BDatabase::objectPropertyChanged, this, [this](quint32 id) {
        if (id == d->id) {
            updateFromDocument();
        }
    });
}
