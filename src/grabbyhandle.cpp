#include <QPainter>

#include "grabbyhandle.h"
#include "paintutils.h"

GrabbyHandle::GrabbyHandle(QQuickItem* parent) : QQuickPaintedItem(parent)
{
    setImplicitSize(16, 16);
    setAcceptedMouseButtons(Qt::LeftButton);
}
GrabbyHandle::~GrabbyHandle()
{

}

void GrabbyHandle::paint(QPainter* painter)
{
    painter->save();

    painter->setPen(Qt::cyan);
    painter->setBrush(Qt::darkMagenta);
    drawRect(painter, boundingRect());

    painter->restore();
}

void GrabbyHandle::mousePressEvent(QMouseEvent* evt)
{
    previousPos = evt->globalPos();
    grabMouse();
}

void GrabbyHandle::mouseMoveEvent(QMouseEvent* evt)
{
    const auto diff = evt->globalPos() - previousPos;
    Q_EMIT moved(QVector2D(diff.x(), diff.y()));
    previousPos = evt->globalPos();
}

void GrabbyHandle::mouseReleaseEvent(QMouseEvent* evt)
{
    ungrabMouse();
}

