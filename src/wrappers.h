#pragma once

#include <QColor>
#include <QRectF>

#include "bdocument.h"

// Shortlived base class for more typesafe property access.
// Shouldn't live for longer than the constructed reference is valid.
struct Wrapper
{

    std::unique_ptr<BDocument>& document;
    quint32 id;

public: 

    Wrapper(std::unique_ptr<BDocument>& document, quint32 id) : document(document), id(id)
    { }

};

#define _wrapperConstructor(name) name(std::unique_ptr<BDocument>& document, quint32 id) : Wrapper(document, id) { }

// wrapper for the topleft and bottomright corners of an object
struct WBounding : public Wrapper
{

    _wrapperConstructor(WBounding)

    qreal topLeftX() { return document->getProperty(id, "tlX").toReal(); }
    qreal topLeftY() { return document->getProperty(id, "tlY").toReal(); }
    qreal bottomRightX() { return document->getProperty(id, "brX").toReal(); }
    qreal bottomRightY() { return document->getProperty(id, "brY").toReal(); }

    void setTopLeftX(qreal value) { document->setProperty(id, "tlX", value); }
    void setTopLeftY(qreal value) { document->setProperty(id, "tlY", value); }
    void setBottomRightX(qreal value) { document->setProperty(id, "brX", value); }
    void setBottomRightY(qreal value) { document->setProperty(id, "brY", value); }

    QRectF rect()
    {
        return QRectF(QPointF(topLeftX(), topLeftY()), QPointF(bottomRightX(), bottomRightY()));
    }
    QRectF zeroZeroRect()
    {
        const auto r = rect();
        return QRectF(QPointF(0, 0), QPointF(r.width(), r.height()));
    }

    void setRect(const QRectF& rect)
    {
        const auto tl = rect.topLeft(),
                   br = rect.bottomRight();

        setTopLeftX(tl.x());
        setTopLeftY(tl.y());
        setBottomRightX(br.x());
        setBottomRightY(br.y());
    }

};

// wrapper for the radius
struct WRadius : public Wrapper
{

    _wrapperConstructor(WRadius)

    qreal radius() { return document->getProperty(id, "r").toReal(); }
    void setRadius(qreal r) { document->setProperty(id, "r", r); }

};

// wrapper for the colour
struct WColor : public Wrapper
{

    _wrapperConstructor(WColor)

    QColor color() { return document->getProperty(id, "c").value<QColor>(); }
    void setColor(const QColor& c) { document->setProperty(id, "c", c); }

};
